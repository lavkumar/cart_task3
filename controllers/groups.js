const UserModel = require('../models/Groups')
const  M = require('../models/Category')
const  N = require('../models/Products')

// Productscontrller
// Create and Save a new user

exports.create_products = (req, res) => {
    // Validate request
    if(!req.body.desc) {
        return res.status(400).send({
            message: "products desc can not be empty"
        });
    }

    // Create a products
    const Model = new N({
        name: req.body.name || "Untitled Note", 
        desc: req.body.desc
    });

    // Save products in the database
    Model.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the products."
        });
    });
};
// retrive all data
exports.findAll_products = (req, res) => {
    N.find()
    .then( Model => {
        res.send( Model);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving."
        });
    });
};



// Categorycontrller
// Create and Save a new user

exports.createdata = (req, res) => {
    // Validate request
    if(!req.body.desc) {
        return res.status(400).send({
            message: "categories desc can not be empty"
        });
    }

    // Create a Category
    const Model = new M({
        name: req.body.name || "Untitled ", 
        desc: req.body.desc
    });

    // Save Note in the database
    Model.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Category."
        });
    });
};
// retrive all data
exports.findAlldata = (req, res) => {
    M.find()
    .then( Model => {
        res.send( Model);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving."
        });
    });
};



// groupcontrller
// Create and Save a new user

exports.create = (req, res) => {
    // Validate request
    if(!req.body.desc) {
        return res.status(400).send({
            message: "group desc can not be empty"
        });
    }

    // Create a Note
    const group = new UserModel({
        name: req.body.name || "Untitled ", 
        desc: req.body.desc
    });

    // Save Group in the database
    group.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });
};
// retrive all data
exports.findAll = (req, res) => {
    UserModel.find()
    .then( UserModel => {
        res.send( UserModel);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving."
        });
    });
};
