var mongoose = require('mongoose');

require('../all-models').toContext(global);


//------------------------
// ADD SEEDS BELOW
//------------------------


// suggested module for generating fake contextual data
// var Faker = require('faker');


// For Example

// CoolUser.create([
//   { name: 'andy', age: 24 },
//   { name: 'alex', age: 23 },
//   { name: Faker.name.firstName(), age: Faker.random.number() }
// ])

// .then(() => {
//   console.log("Seed complete!")  
//   mongoose.connection.close();
// });

// be sure to close the connection once the queries are done

// Groups.create([
//   { name: 'andy', desc:'good' },
//   { name: 'alex', desc: 'good' },
  
// ])

// .then(() => {
//   console.log("Seed complete!")  
//   mongoose.connection.close();
// });
// Category.create([
//     { name: 'andy', desc:'good'},
//     { name: 'alex', desc:'good'}
    
//   ])
  
//   .then(() => {
//     console.log("Seed complete!")  
//     mongoose.connection.close();
//   });
Products.create([
  { name: 'andy', desc:'good',image_url:'http/image'},
  { name: 'alex', desc:'good',image_url:'http/image'}
  
])

.then(() => {
  console.log("Seed complete!")  
  mongoose.connection.close();
});

